#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <cstdlib>
#include "mpi.h"
#include "../Tools.h"

using namespace std;

const int FUNC_LEN = 512;

void to_function(vector<string> rules, int *function);
void steps(int **local_config, int local_n, int conf_size, int *function, int rank, int pcount, int iterations);
int toDec(int binary[9]);

int main(int argc, char *argv[]) {

    if(argc < 4) {
        throw invalid_argument("Missing arguments, correct Usage: ./Cellular2D-Parallel gameOfLife.txt config.txt t\n");
    }

    int pcount, rank;
    int t = stoi(argv[3]);

    double local_start, local_finish, local_elapsed, elapsed;

    int conf_size;            
    int *config, *local_config;
    int *function = new int[FUNC_LEN];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &pcount);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int *sendcounts = new int[pcount];
    int *displs = new int[pcount];

    if (rank == 0) {

        vector<string> rules = Tools::readFile(argv[1]);
        to_function(rules, function);

        //conf_size = stoi(argv[2]);
        //vector<vector<int>> cfg = Tools::life_generator(conf_size);

        vector<string> configfile = Tools::readFile(argv[2]);
        conf_size = stoi(configfile[0]);

        config = new int[conf_size * conf_size];

        for (int i = 1; i <= conf_size; i++) {
            for (int j = 0; j < conf_size; j++) {
                config[(i - 1) * conf_size + j] = configfile[i][j] == '1' ? 1 : 0;
                //config[(i - 1) * conf_size + j] = cfg[i - 1][j];
            }
        }

        int rem = conf_size % pcount;
        int sum = 0;

        for (int i = 0; i < pcount; i++) {
            sendcounts[i] = conf_size * (conf_size / pcount);
            if (rem > 0) {
                sendcounts[i] += conf_size;
                rem--;
            }

            displs[i] = sum;
            sum += sendcounts[i];
        }  

    }

    MPI_Bcast(&conf_size, 1, MPI_INT, 0, MPI_COMM_WORLD);

    //Broadcast the transformation function
    MPI_Bcast(function, FUNC_LEN, MPI_INT, 0, MPI_COMM_WORLD);

    //Brodcast the sendcounts for each process to have a properly sized receive buffer.
    MPI_Bcast(sendcounts, pcount, MPI_INT, 0, MPI_COMM_WORLD); 

    int local_n = sendcounts[rank];
    local_config = new int[local_n];

    MPI_Barrier(MPI_COMM_WORLD);
    local_start = MPI_Wtime();

    MPI_Scatterv(config, sendcounts, displs, MPI_INT, local_config, local_n, MPI_INT, 0, MPI_COMM_WORLD);

    steps(&local_config, local_n, conf_size, function, rank, pcount, t);

    MPI_Gatherv(local_config, local_n, MPI_INT, config, sendcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);

    local_finish = MPI_Wtime();
    local_elapsed = local_finish - local_start;
    MPI_Reduce(&local_elapsed, &elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

    MPI_Finalize();

    if (rank == 0) {
        printf("%.4f", elapsed);
    }

    return 0;
}


void steps(int **local_config, int local_n, int conf_size, int *function, int rank, int pcount, int iterations) {

    int rows = local_n / conf_size;
    int up, down;
    int *next_it = new int[local_n];
    int from_up[conf_size], from_down[conf_size];

    for (int i = 0; i < iterations; i++) {

        up = Tools::mod(rank - 1, pcount);
        down = Tools::mod(rank + 1, pcount);


        if (pcount > 1 && rank % 2 == 0) {
            MPI_Send(*local_config, conf_size, MPI_INT, up, 0, MPI_COMM_WORLD);
            MPI_Recv(&from_down, conf_size, MPI_INT, down, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            if (pcount % 2 != 0 && rank == pcount - 1 && rank != 0) {     
                MPI_Recv(&from_up, conf_size, MPI_INT, up, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Send(*(local_config) + local_n - conf_size, conf_size, MPI_INT, down, 0, MPI_COMM_WORLD);
            } else {
                MPI_Send(*(local_config) + local_n - conf_size, conf_size, MPI_INT, down, 0, MPI_COMM_WORLD);
                MPI_Recv(&from_up, conf_size, MPI_INT, up, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        } else if (pcount > 1) {
            MPI_Recv(&from_down, conf_size, MPI_INT, down, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(*local_config, conf_size, MPI_INT, up, 0, MPI_COMM_WORLD);
            
            MPI_Recv(&from_up, conf_size, MPI_INT, up, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(*(local_config) + local_n - conf_size, conf_size, MPI_INT, down, 0, MPI_COMM_WORLD);
        } else if (pcount == 1) {
            
            copy(*local_config, (*local_config) + conf_size, from_down);
            copy(*(local_config) + local_n - conf_size, *(local_config) + local_n, from_up);

        }

        int binary[9];
        int index = 0; 

        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < conf_size; x++) {

                int idx[3] = {Tools::mod(x - 1, conf_size), x, Tools::mod(x + 1, conf_size)};
                int idy[3] = {Tools::mod(y - 1, rows), y, Tools::mod(y + 1, rows)};
                int bi = 0;

                for (int dy = 0; dy < 3; dy++) {
                    for (int dx = 0; dx < 3; dx++) {
                        
                        if (y == 0 && dy == 0) {
                            binary[bi] = from_up[idx[dx]];
                        } else if (y == rows - 1 && dy == 2) {
                            binary[bi] = from_down[idx[dx]];
                        } else {
                            binary[bi] = (*local_config)[idy[dy] * conf_size + idx[dx]];
                        }
                        bi++;
                    }
                }
                next_it[index++] = function[toDec(binary)];
            }
        }

        int *tmp = *local_config;
        *local_config = next_it;
        next_it = tmp;
    }
}


int toDec(int binary[9]) {

    int index = 0;
    int pow2 = 1;

    for (int i = 8; i >= 0; i--) {
        index += binary[i] * pow2;
        pow2 *= 2;
    }

    return index;
}

/*
    Assume lines contain strings of the form:
    0123456789 10
    bbbbbbbbb  b
    where b are bits, and bbbbbbbbb are in increasing order s.t. 
*/
void to_function(vector<string> rules, int *function) {
    for (int i = 0; i < rules.size(); i++) {
        function[i] = rules[i][10] == '1' ? 1 : 0;
    }
}


/*
Copyright 2019 Amund Lindberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

