#ifndef Tools_H
#define Tools_H

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <string>
#include <stdexcept>
#include <stdlib.h>

using namespace std;

class Tools {

    Tools();

public:
    static int mod(int a, int b);
    static string toBinary(int n, int p);
    static int toDecimal(string bits);

    static vector<string> readFile(string filename);
    static void writeFile(vector<string> content, string filename);
    static void printContent(vector<string> content);

    static string middle_config(int k);
    static vector<vector<int>> life_generator(int size);
};

#endif