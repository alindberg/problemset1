#import os.. path?
import matplotlib.pyplot as plt

rows = []
with open('matrix.txt') as fp:
    for row in fp:
        line = []
        for c in row.rstrip('\n'):
            line.append(int(c))
        rows.append(line)


plt.plot(1)
plt.imshow(rows, cmap='Greys', interpolation='nearest')
plt.savefig('matrix.png')

plt.show()