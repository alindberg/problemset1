#include "Tools.h"

int Tools::mod(int a, int b) {
    return (a % b + b) % b; 
}

int Tools::toDecimal(string bits) {
    int n = bits.size();

    int dec = 0;
    
    for (int i = 0; i < n; i++) {
        if (bits[i] == '1') {
            dec += pow(2, n - i - 1);
        }
    }
    return dec;
}

/**
 * Convert decimal n < 2^power to a binary string. 
 * length of string is power + 1.
*/
string Tools::toBinary(int n, int power) {
    string s;

    for(int p = power; p >= 0; p--) {
        int c = pow(2, p);
        if (n >= c) {
            s.push_back('1');
            n -= c;
        } else {
            s.push_back('0');
        }
    }
    return s;
}


string Tools::middle_config(int k) {
    return string(pow(2, k), '0') + '1' + string(pow(2, k), '0');
}

vector<vector<int>> Tools::life_generator(int size) {

    vector<vector<int>> life(size, vector<int>(size));
    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            life[y][x] = rand() % 2;
        }
    }
    return life;
}


vector<string> Tools::readFile(string filename) {
    ifstream in(filename); 

    if(!in) {
        throw invalid_argument("Cant open file " + filename);
    }

    vector<string> lines;

    string line;
    while(getline(in, line)) {
        if(line.size() > 0) {
            lines.push_back(line);
        }
    }

    in.close();
    return lines;
}


void Tools::writeFile(vector<string> content, string filename) {
    ofstream out(filename);

    if(!out) {
        throw invalid_argument("Cant write to file " + filename);
    } 

    for(string line: content) {
        out << line << endl;
    }

    out.close();
}

void Tools::printContent(vector<string> content) {
    for(string line: content) {
        cout << line << endl;
    }
}