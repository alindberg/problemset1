#include <iostream>
#include <stdlib.h>
#include <fstream>

using namespace std;

int main() {

    int width, height;

    width = 4;
    height = 4;

    ofstream outputFile;
    outputFile.open("config4.txt");

    outputFile << width << endl;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int bit = rand() % 2;
            outputFile << bit;
        }
        outputFile << endl;
    }

    outputFile.close();
    return 0;
}