#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <sstream>
#include <cmath>
#include "../Tools.h"

using namespace std;

vector<string> split(string s) {

    vector<string> result;
    istringstream iss(s);

    for(string s; iss >> s;) {
        result.push_back(s);
    }

    return result;
}


int main(int argc, char *argv[]) {

    if(argc < 3) {
        cout << "Missing arguments, correct Usage: ./Branching-Sequential bp.txt assignment" << endl;
        return 0;
    }

    //READ ALL INPUT

    vector<string> lines = Tools::readFile(argv[1]);
    int curline = 0;

    int n = stoi(lines[curline++]);
    int m = stoi(lines[curline++]);
    int k = stoi(lines[curline++]);
    int r = stoi(lines[curline++]);

    //cout << "n = " << n << ", m = " << m << ", k = " << k << ", r = " << r << endl;

    int *function = new int[k * k * k];
    int *triples = new int[m * 3];
    int *fin = new int[r]; 

    vector<string> spl;

    
    for (int i = 0; i < k * k; i++) {
        spl = split(lines[curline++]);
        for (int j = 0; j < k; j++) {
            function[i * k + j] = stoi(spl[j]);
            //cout << function[i * k + j] << " ";
        }
        //cout << endl;
    }

    for (int i = 0; i < r; i++) {
        fin[i] = stoi(lines[++curline]);
        //cout << fin[i] << " ";
    }
    //cout << endl;


    int l = 0; 
    for (int i = 0; i < m; i++) {
        spl = split(lines[curline++]);
        for (int j = 0; j < 3; j++) {
            triples[l++] = stoi(spl[j]);
            //cout << triples[i * m + j] << " ";
        }
        //cout << endl;
    }

    int *assignment = new int[n];
    for (int i = 0; i < n; i++) {
        assignment[i] = argv[2][i] == '1' ? 1 : 0;
    }

    //From here I assume n = m

    int result = 0;
    for (int i = 0; i < n; i++) {
        int Ia = triples[i * 3 + assignment[i] + 1];

        if (i == 0) {
            result = Ia;
        } else {
            int fx = (result + Ia) % 3; //Here is supposed to be a lookup in the function array. my test function was mod 3.
            result = fx;
        }
        cout << "result = " << result << ", Ia = " << Ia << endl;
    }

    //If the result is found, the answer is 1, otherwise 0.
    bool found = false;
    for (int y = 0; y < r; y++) {
        if (fin[y] == result) {
            found = true;
            break;
        } 
    }

    cout << (found ? 1 : 0) << endl;



    return 0;
}

/*
Copyright 2019 Amund Lindberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
