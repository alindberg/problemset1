#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include "mpi.h"
#include <stdexcept>
#include "../Tools.h"

using namespace std;

const int FUNC_LEN = 8;

void to_function(vector<string> rules, char *function);
void create_local_history(char *local_hist, char *local_config, int local_n, char *function, int rank, int pcount, int iterations);

int main(int argc, char *argv[]) {

    if(argc < 4) {
        throw invalid_argument("Missing arguments, correct Usage: ./Cellular1D-Parallel mod2.txt config.txt t\n");
    }

    int pcount, rank;
    int t = stoi(argv[3]);

    double local_start, local_finish, local_elapsed, elapsed;

    int conf_size;            
    char *config;
    char *function = new char[FUNC_LEN];

    char *local_hist, *history;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &pcount);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int *sendcounts = new int[pcount];
    int *displs = new int[pcount];

    if (rank == 0) {

        vector<string> rules = Tools::readFile(argv[1]);
        to_function(rules, function);

        vector<string> configfile = Tools::readFile(argv[2]);
        int k = stoi(configfile[0]);
        string gc = configfile[1]; 

        //int k = stoi(argv[2]);
        //string gc = Tools::middle_config(k);
        
        conf_size = 2 * pow(2, k) + 1;
        config = new char[conf_size + 1]; //+1 for null terminator
        
        //Problem with first string if config = &gc[0]
        
        for (int l = 0; l < conf_size; l++) {
            config[l] = gc[l];
        }
        
        config[conf_size] = '\0';

        int rem = conf_size % pcount;
        int sum = 0;

        for (int i = 0; i < pcount; i++) {
            sendcounts[i] = conf_size / pcount;
            if (rem > 0) {
                sendcounts[i]++;
                rem--;
            }

            displs[i] = sum;
            sum += sendcounts[i];
        }   

        history = new char[(t + 1) * conf_size];
    }

    MPI_Bcast(&conf_size, 1, MPI_INT, 0, MPI_COMM_WORLD);

    //Broadcast the transformation function
    MPI_Bcast(function, FUNC_LEN, MPI_CHAR, 0, MPI_COMM_WORLD);

    //Brodcast the sendcounts for each process to have a properly sized receive buffer.
    MPI_Bcast(sendcounts, pcount, MPI_INT, 0, MPI_COMM_WORLD); 

    int local_n = sendcounts[rank];
    char *local_config = new char[local_n];

    MPI_Barrier(MPI_COMM_WORLD);
    local_start = MPI_Wtime();

    //Scatter the config into the local receive buffers.
    MPI_Scatterv(config, sendcounts, displs, MPI_CHAR, local_config, local_n, MPI_CHAR, 0, MPI_COMM_WORLD);

    local_hist = new char[(t + 1) * local_n];

    create_local_history(local_hist, local_config, local_n, function, rank, pcount, t); 

    for (int i = 0; i <= t; i++) {
        MPI_Gatherv(local_hist + i * local_n, local_n, MPI_CHAR, history + i * conf_size, sendcounts, displs, MPI_CHAR, 0, MPI_COMM_WORLD);
    }

    local_finish = MPI_Wtime();
    local_elapsed = local_finish - local_start;
    MPI_Reduce(&local_elapsed, &elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);


    MPI_Finalize();

    if (rank == 0) {
        printf("%.4f", elapsed);

    /*
        for (int y = 0; y <= t; y++) {

            for (int x = 0; x < conf_size; x++) {
                cout << history[y * conf_size + x];
            }
            cout << endl;
        }
    */
    }

    delete[] sendcounts;
    delete[] displs;
    delete[] local_config;
    delete[] function;
    if(rank == 0) {
        //delete[] config;
    }
    
}


void create_local_history(char *local_hist, char *local_config, int local_n, char *function, int rank, int pcount, int iterations) {
    
    char first, last;
    int left, right;
    
    for (int k = 0; k < local_n; k++) {
        local_hist[k] = local_config[k];
    }

    for (int i = 1; i <= iterations; i++) {

        left = Tools::mod(rank - 1, pcount);
        right = Tools::mod(rank + 1, pcount);

        int prev_row = local_n * (i - 1); //0th element of the previous row
        int cur_row = local_n * i;

        if (rank % 2 == 0) {

            MPI_Send(&local_hist[prev_row], 1, MPI_CHAR, left, 0, MPI_COMM_WORLD);
            MPI_Recv(&first, 1, MPI_CHAR, left, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            
            if (pcount % 2 != 0 && rank == pcount - 1 && rank != 0) {  
                MPI_Recv(&last, 1, MPI_CHAR, right, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Send(&local_hist[prev_row + local_n - 1], 1, MPI_CHAR, right, 0, MPI_COMM_WORLD);
            } else {
                MPI_Send(&local_hist[prev_row + local_n - 1], 1, MPI_CHAR, right, 0, MPI_COMM_WORLD);
                MPI_Recv(&last, 1, MPI_CHAR, right, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }

        } else {
            MPI_Recv(&last, 1, MPI_CHAR, right, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(&local_hist[prev_row + local_n - 1], 1, MPI_CHAR, right, 0, MPI_COMM_WORLD);
            
            MPI_Recv(&first, 1, MPI_CHAR, left, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(&local_hist[prev_row], 1, MPI_CHAR, left, 0, MPI_COMM_WORLD);
        }

        if (local_n > 1) {

            string bits = {first, local_hist[prev_row], local_hist[prev_row + 1]};
            local_hist[cur_row] = function[Tools::toDecimal(bits)];

            for (int j = 1; j < local_n - 1; j++) {
                bits = {local_hist[prev_row + j - 1], local_hist[prev_row + j], local_hist[prev_row + j + 1]};
                local_hist[cur_row + j] = function[Tools::toDecimal(bits)];
            }

            bits = {local_hist[prev_row + local_n - 2], local_hist[prev_row + local_n - 1], last};
            local_hist[cur_row + local_n - 1] = function[Tools::toDecimal(bits)];

        //Handle the case where the process got a local string of length 1..
        } else if (local_n == 1) {
            string bits = {first, local_hist[prev_row], last};
            local_hist[cur_row] = function[Tools::toDecimal(bits)];
        } else {
            throw invalid_argument("A process got no local string");
        }
        
    }
}

/*
    Assume rules contain 8 strings of the form:
    01234
    bbb b
    where b are bits, and bbb are in order.
*/
void to_function(vector<string> rules, char *function) {

    for (int i = 0; i < rules.size(); i++) {
        function[i] = rules[i][4];
    }
}

/*
Copyright 2019 Amund Lindberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


