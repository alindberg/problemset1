#!/bin/bash
 
#module load mpi/openmpi-x86_64
mpic++ -std=c++11 2-Parallel/Cellular2D-Parallel.cpp -O3 -w -o Cellular2D-Parallel Tools.cpp

rm results2D.txt
 
N=( 1024 2048 4096 ) #8192 16384  32768 65536 )
processes=( 1 ) #4 8 16 32 64 128  256 512 )

echo Running 2D Parallel
 
printf "k \\textbackslash p" >> results2D.txt
printf " & %s" "${N[@]}" >> results2D.txt
printf " [0.5ex]%s%s" "\\" "\\" >> results2D.txt

for p in "${processes[@]}"
do
    result=()
    for i in "${N[@]}"
    do
        echo Now running with p = $p and N = $i with 10000 iterations
        result=("${result[@]}" $(mpirun -np "$p" -quiet -q ./Cellular2D-Parallel rules/gameOfLife.txt $i 10000))
    done
    printf "\n$p" >> results2D.txt
    printf " & %s" "${result[@]}" >> results2D.txt
    printf "%s%s" "\\" "\\" >> results2D.txt
done

