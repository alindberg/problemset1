#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include "Tools.h"

using namespace std;

char alive(string binary) {

    int middle = 4;
    int neighbours = 0;

    for (int i = 0; i < binary.size(); i++) {
        if (i == middle) continue;

        if (binary[i] == '1') {
            neighbours++;
        }
    }

    char c;
    if (binary[middle] == '1') {
        c = neighbours == 2 || neighbours == 3 ? '1' : '0';
    } else {
        c = neighbours == 3 ? '1' : '0';
    }

    return c;
}

int main() {

    ofstream outputFile;
    outputFile.open("gameOfLife.txt");

    int maxpow = 9;

    for (int i = 0; i < pow(2, maxpow); i++) {

        string k = Tools::toBinary(i, maxpow - 1);

        outputFile << k << " " << alive(k) << endl;
    }

    outputFile.close();

    return 0;
}


