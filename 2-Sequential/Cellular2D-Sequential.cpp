#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <stdexcept>
#include "../Tools.h"

using namespace std;

/*
    Assume lines contain strings of the form:
    0123456789 10
    bbbbbbbbb  b
    where b are bits, and bbbbbbbbb are in increasing order s.t. 
    toDecimal(bbbbbbbbb) would be an index in func
*/
string toFunction(vector<string> rules) {
    string func;

    for(string rule: rules) {  
        func += rule[10];
    }

    return func;
}

string neighbours(int x, int y, vector<string> config, int n) {

    string bits;

    for (int dy = y - 1; dy <= y + 1; dy++) {
        for (int dx = x - 1; dx <= x + 1; dx++) {
            bits += config[Tools::mod(dy, n)][Tools::mod(dx, n)];
        }  
    }

    return bits;        
}


vector<string> nextConfig(vector<string> config, int n, string func) {
    vector<string> next_config(n);
    string neighbour_bits;
    int index;

    for (int y = 0; y < n; y++) {
        string s;
        for (int x = 0; x < n; x++) {

            neighbour_bits = neighbours(x, y, config, n);
            index = Tools::toDecimal(neighbour_bits);
            s += func[index];
        }

        next_config[y] = s;
    }

    return next_config;
}

int main(int argc, char *argv[]) {

    if(argc < 4) {
        cout << "Missing arguments, correct Usage: ./Cellular2D-Sequential gameOfLife.txt config.txt t" << endl;
        return 0;
    }

    vector<string> funcfile = Tools::readFile(argv[1]);
    string func = toFunction(funcfile);

    vector<string> config = Tools::readFile(argv[2]);
    int n = stoi(config[0]);
    config.erase(config.begin());

    int t = stoi(argv[3]);

    for (int i = 0; i < t; i++) {
        config = nextConfig(config, n, func);

        Tools::printContent(config);
        cout << endl;
    }

    return 0;
}

/*
Copyright 2019 Amund Lindberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

