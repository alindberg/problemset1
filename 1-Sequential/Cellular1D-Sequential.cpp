#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <cmath>
#include "../Tools.h"

using namespace std;

/*
    Assume rules contain strings of the form:
    01234
    bbb b
    where b are bits, and bbb are in order.
*/
string toFunction(vector<string> rules) {
    string func;

    for(string rule: rules) {  
        func += rule[4];
    }
    return func;
}

string nextConfig(string cfg, string func) {
    int n = cfg.size();

    string next;
    for (int i = 0; i < n; i++) {
        string bits = {cfg[Tools::mod(i - 1, n)], cfg[i], cfg[Tools::mod(i + 1, n)]};

        char fx = func[Tools::toDecimal(bits)];
        next += fx;
    }
    return next;
}

int main(int argc, char *argv[]) {

    if(argc < 4) {
        cout << "Missing arguments, correct Usage: ./Cellular1D-Sequential mod2.txt middle30.txt t" << endl;
        return 0;
    }

    vector<string> rules = Tools::readFile(argv[1]);
    string function = toFunction(rules);

    vector<string> configfile = Tools::readFile(argv[2]);
    string config = configfile[1];

    int t = stoi(argv[3]);

    //vector<string> configs(t + 1);
    //configs[0] = config;

   // cout << config << " (i = " << 0 << ")" << endl;
    
    for (int i = 1; i <= t; i++) {
        config = nextConfig(config, function);
        //configs[i] = config;
       //cout << config << " (i = " << i << ")" << endl;
    }

    //Tools::writeFile(configs, "matrix.txt");

    return 0;
}

/*
Copyright 2019 Amund Lindberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
