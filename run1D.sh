#!/bin/bash
 
#module load mpi/openmpi-x86_64
mpic++ -std=c++11 1-Parallel/Cellular1D-Parallel.cpp -O3 -w -o Cellular1D-Parallel Tools.cpp

rm results1D.txt
 
processes=(1)
k=(7 8 9 10)

printf "p \\textbackslash k" >> results1D.txt
printf " & %s" "${processes[@]}" >> results1D.txt
printf "%s%s[0.5ex]" "\\" "\\" >> results1D.txt

echo Running 1D Parallel

for i in "${k[@]}"
do
    result=()
    for p in "${processes[@]}"
    do
        echo Now running with k = $i and p = $p with 1000000 iterations
        result=("${result[@]}" $(mpirun -np "$p" -quiet -q ./Cellular1D-Parallel rules/mod2.txt $i 1000000))
    done
    printf "\n$i" >> results1D.txt
    printf " & %s" "${result[@]}" >> results1D.txt
    printf "%s%s" "\\" "\\" >> results1D.txt
done
